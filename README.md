Assignment 1 This file will explain how to set up and run the project

This project is to test funcionality of BitBucket.

Bitbucket Startup

Sign up for Bitbucket. Create a public repository with source code from a previous assignment. Fill out the attached html link to your repository and submit it in the assignment folder.	50
Add an issue tracker and create an issue of something that you didn't quite get working or of an enhancement that you would like to make.	10
Create a README.md file with instructions on how to build install and use your project.	10
Add a license for your project and explain your choice in a note in the README.md file.	10
Add a project Wiki and write an article that explains why a CAPSTONE customer might want you to use a private repository rather than a public one. Contrast the CAPSTONE customer's needs with the needs of Signal Private Messenger and describe why they would be open source.	10
Make a change to your README.md file, commit it and push to bitbucket. Then revert the commit and push that to Bitbucket as well.	10

This project is under the MIT license and is based on open source, so I am totally fine with someone reusing or modifying my code.

Revert